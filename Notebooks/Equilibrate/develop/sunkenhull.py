import numpy as np
from os import path
import pandas as pd
import scipy.optimize as opt
from scipy import optimize
import scipy.linalg as lin
import scipy as sp
import sys
import sympy as sym

from collections import OrderedDict as odict

from thermoengine import coder, core, phases, model, equilibrate

import ternary

#################################
#  Immiscible phase separation  #
#################################

def min_soln_energy(Phs, dmu, T, P, mu_endmem, X_bulk, X_init=None):

    #A, X_min, output = Phs.affinity_and_comp(T, P, dmu+mu_endmem, X_init=X_init, ATOL=1e-6,
    #                                         full_output=True, save_hist=save_hist)
    mu = dmu+mu_endmem
    mu[mu_endmem==0] = 0
    # print('mu = ', mu)
    A, X_min = Phs.affinity_and_comp(T, P, dmu+mu_endmem, X_init=X_init, ATOL=1e-6)

    dmu_min = Phs.chem_potential(T, P, mol=X_min).squeeze()-mu_endmem

    dmu_min[mu_endmem==0] = 0
    #dmu_hist = output['m u_hist']-mu_endmem[np.newaxis,:]
    #X_hist = output['X_hist']
    #dG_hist = np.hstack([np.dot(idmu, iX) for idmu, iX in zip(dmu_hist, X_hist)])

    dG = np.dot(dmu_min, X_min)

    # output['dG_hist'] = dG_hist
    # output['dmu_hist'] = dmu_hist

    #return dG, dmu_min, X_min, output
    return dG, dmu_min, X_min

def hull_finder(xconstr, x, z, Ndim=None, dztol=1e-4, ERRTOL = 1e-10):
    if Ndim is None:
        Ndim = xconstr.size

    z_lims = [z.min(), z.max()]

    dz = dztol*np.diff(z_lims)
    xz_lib = np.hstack((x, z[:, np.newaxis]))

    z_target = z_lims[0]

    nnls_err0 = 1e3*ERRTOL
    while nnls_err0 > ERRTOL:
        z_target, nnls_err0, wts0 = _update_z_target(xconstr, z_target, xz_lib, dz)


    # wt_mask = wts0>WTTOL
    inds = np.argsort(wts0)[::-1]
    ind_hull = inds[:Ndim]


    xz_hull = xz_lib[ind_hull,:]

    x_hull = xz_hull[:,:-1]
    z_hull = xz_hull[:,-1]
    output = np.linalg.lstsq(x_hull.T, xconstr, rcond=None)
    wt_hull_only = output[0]

    wt_hull = np.zeros(z.size)
    wt_hull[ind_hull] = wt_hull_only

    zconstr = np.dot(wt_hull, z)
    hull = {}
    hull['x'] = x_hull
    hull['z'] = z_hull
    hull['ind'] = ind_hull

    return wt_hull, zconstr, hull

def _update_z_target(xconstr, z_target0, xz_lib, dz):
    xz_target0 = np.hstack(( xconstr, z_target0))
    output0 = opt.nnls(xz_lib.T, xz_target0)
    nnls_err0 = output0[1]
    wts0 = output0[0]

    xz_target1 = np.hstack(( xconstr, z_target0-dz))

    output1 = opt.nnls(xz_lib.T, xz_target1)
    nnls_err1 = output1[1]

    dz_target = dz/(nnls_err1-nnls_err0)*nnls_err0

    z_target = z_target0+dz_target[0]
    return z_target, nnls_err0, wts0

def plot_assem(X_curr, title, ax):
    points = 100*X_curr[:,::-1]

    scale = 100
    ax.cla()
    figure, tax = ternary.figure(ax=ax, scale=scale)
    figure.set_size_inches(5, 5)
    # Plot a few different styles with a legend
    # points = random_points(30, scale=scale)
    #tax.scatter(100*tieline['mol_edges'], marker='s', color='red', label="Red Squares")

    tax.scatter(points, marker='s', color='red')
    #points = random_points(30, scale=scale)
    #tax.scatter(points, marker='D', color='green', label="Green Diamonds")
    tax.legend()
    tax.set_title(title, fontsize=20)
    tax.boundary(linewidth=2.0)
    tax.gridlines(multiple=10, color="gray")
    tax.ticks(axis='lbr', linewidth=1, multiple=10)
    tax.clear_matplotlib_ticks()
    tax.get_axes().axis('off')

    tax.show()

def discover_new_compounds(dmu_target, Phs, T, P, X_bulk,
                           assem, history, Nc=None,
                           plot_ax=None, WTTOL=1e-4, debug=False):

    # X_hist = history['X']
    # dG_hist = history['dG']
    # dmu_hist = history['dmu']
    mu_endmem = history['mu_endmem']

    if Nc is None:
        Nc = Phs.endmember_num

    TOL=1e-4
    dG_new = 0
    for i in range(Nc):
        #idG_new, idmu_new, iX_new, ioutput = min_soln_energy(
        #    dmu_target, X_init=X_curr[i], save_hist=False)
        idG_new, idmu_new, iX_new = min_soln_energy(
            Phs, dmu_target, T, P, mu_endmem, X_bulk, X_init=assem['X'][i])

        # min_soln_energy(dmu, T, P, mu_endmem, X_bulk, X_init=None)
        # print(ioutput['X_hist'])
        # print(ioutput['dmu_hist'])
        # print(ioutput['dG_hist'])
        history['X'].append(iX_new)
        history['dG'].append(idG_new)
        history['dmu'].append(idmu_new)

        #X_hist.append(ioutput['X_hist'])
        #dG_hist.append(ioutput['dG_hist'])
        #dmu_hist.append(ioutput['dmu_hist'])

    # print('dmu_hist = ', dmu_hist)
    # idG_new, idmu_new, iX_new = min_soln_energy(dmu_target, X_init=X_bulk)
    # X_hist.append(iX_new)
    # dG_hist.append(idG_new)
    # dmu_hist.append(idmu_new)

    return history

def update_assem(X_bulk, history, Nc, dmu_target=None,
                 prevwt=.3, debug=False):

    dG_hist_arr = np.hstack(history['dG'])
    X_hist_arr = np.vstack(history['X'])
    dmu_hist_arr = np.vstack(history['dmu'])
    mu_endmem = history['mu_endmem']

    wt_hull, dG_bulk, hull = hull_finder(X_bulk, X_hist_arr, dG_hist_arr,
                                         Ndim=Nc)

    #print(wt_hull)
    #print(hull['x'])
    #print(dG_bulk)

    assem = {}

    assem['X'] = hull['x']
    assem['dG'] = hull['z']
    assem['dmu'] = dmu_hist_arr[hull['ind']]
    assem['dG_bulk'] = dG_bulk
    wt_curr = wt_hull[hull['ind']]

    dG_curr_alt = [np.dot(idmu,iX) for iX,idmu
                   in zip(assem['X'], assem['dmu'])]
    #X_curr = X_hist[wt_hull>WTTOL,:]
    #dG_curr = dG_hist[wt_hull>WTTOL]
    if debug:
        print('X_curr = ', assem['X'])
        print('dmu_curr = ', assem['dmu'])
        print('dG_curr = ', assem['dG'])
        print('dG_curr_alt = ', dG_curr_alt)
        print('dG_bulk = ', assem['dG_bulk'])

    dmu_new = np.linalg.lstsq(assem['X'], assem['dG'], rcond=None)[0]
    #dmu_avg = np.mean(dmu_curr, axis=0)

    # dmu_wtavg = np.dot(wt_curr,dmu_curr)
    # dmu_avg = np.mean(dmu_curr, axis=0)
    # dmu_new = dmu_avg
    # print(dmu_wtavg)
    #dmu_new = np.linalg.lstsq(X_curr.T,hull['z'], rcond=None)[0]


    if dmu_target is None:
        dmu_target = dmu_new
    else:
        dmu_target = prevwt*dmu_target + (1-prevwt)*dmu_new
    # dmu_target = dmu_new


    dmu_target[mu_endmem==0] = 0


    return dmu_target, assem

def separate_phase(Phs, T, P, mol,
                   init_history=None, zero_component=None,
                   XTOL=1e-2, debug=False,
                   Niter=50, prevwt=0.3):
    Nc = Phs.endmember_num
    mu_endmem = Phs.gibbs_energy(np.full(Nc,T), np.full(Nc,P), mol=np.eye(Nc))
    if zero_component is not None:
        #Nc = np.sum(1.0*zero_component)
        mu_endmem[zero_component] = 0.0

    mu_bulk = Phs.chem_potential(T, P, mol=mol).squeeze()
    dmu_bulk = mu_bulk-mu_endmem
    dG_bulk = np.dot(dmu_bulk, mol)
    X_bulk = mol


    if debug:
        print('dG_bulk = ', dG_bulk)
        print('dmu_bulk = ', dmu_bulk)

    # Switch to true species comp (not just endmembers)
    X_spec = np.eye(Nc)
    # bad names
    mu_spec = Phs.gibbs_energy(np.full(Nc,T), np.full(Nc,P), mol=X_spec)
    # dmu_spec = mu_spec-mu_endmem[np.newaxis,:]

    dG_spec = np.array([np.dot(imu_spec-mu_endmem, iX_spec)
                        for imu_spec, iX_spec in zip(mu_spec, X_spec)])

    mu_spec = np.vstack([Phs.chem_potential(T, P, mol=iX_spec)
                    for iX_spec in X_spec])

    dmu_spec = mu_spec - mu_endmem[np.newaxis,:]



    # X_curr =
    # dG_curr =
    # dmu_curr =

    dmu_target = np.zeros(Nc)
    history = {}
    history['X'] = [X_spec.copy()]
    history['dG'] = [np.zeros(Nc)]
    history['dmu'] = [dmu_spec.copy()]
    history['mu_endmem'] = mu_endmem


    dmu_target, assem, history = iterate_separate_phase(
        Phs, T, P, X_bulk, history, dmu_target=dmu_target,
        prevwt=prevwt, Niter=Niter, debug=debug)

    return dmu_target, assem, history

def iterate_separate_phase(Phs, T, P, X_bulk, history, Nc=None, dmu_target=None,
                           prevwt=0.3, Niter=50, debug=False):

    dmu_target, assem = update_assem(
        X_bulk, history, Nc=Nc, dmu_target=dmu_target, prevwt=0)

    if debug:
        print(assem)
        print(dmu_target)

    iter = 0


    # fig = plt.figure()
    # ax = plt.gca()
    ax=None
    #for i in range(3):

    while iter < Niter:

        history = discover_new_compounds(
            dmu_target, Phs, T, P, X_bulk, assem, history, Nc=Nc,
            debug=False)

        dmu_target, assem = update_assem(
            X_bulk, history, Nc=Nc, dmu_target=dmu_target, prevwt=prevwt)

        if ax is not None:
            plot_assem(X_curr, dG_bulk, ax)
            #print(dG_new)
        iter +=1

        dmu_dev = assem['dmu']-np.mean(assem['dmu'], axis=0)
        rms_dev = np.sqrt(np.mean(dmu_dev**2, axis=0))
        if debug:
            print('iter = ', iter)
            print('rms dev(dmu) = ', rms_dev)


    return dmu_target, assem, history


###########################
#   OLD sunken hull code  #
###########################
def get_subsolidus_phases(database='Berman', include_metal=False,
                          return_model=False):
    remove_phases = ['Liq','H2O']

    if not include_metal:
        remove_phases.extend(['MtlS', 'MtlL'])

    modelDB = model.Database(database)

    phases = modelDB.phases
    # [phases.pop(phs) for phs in remove_phases]

    if return_model:
        return phases, modelDB
    else:
        return phases

def system_energy_landscape(T, P, phases, prune_polymorphs=True, TOL=1e-3,
                            debug=False):
    elem_comps = []
    phs_sym = []
    endmem_ids = []
    endmem_names = []
    mu = []
    phase_comps = odict()
    phase_endmems = odict()
    for phsnm in phases:
        if debug:
            print('=====')
            print(phsnm)
            print('---')
        phs = phases[phsnm]

        iphs_details = odict()

        # elem_comp = phs.props['element_comp']
        abbrev = phs.abbrev

        try:
            elem_comp = phs.props['species_elms']
            endmem_num = phs.props['species_num']
            iendmem_names = list(phs.props['species_name'])

        except:
            elem_comp = phs.props['element_comp']
            endmem_num = phs.endmember_num
            iendmem_names = list(phs.endmember_names)

        iendmem_ids = list(np.arange(endmem_num))

        imu = []

        if phs.phase_type=='pure':
            nelem = np.sum(elem_comp)
            atom_num = np.array([nelem])
            imu += [phs.gibbs_energy(T, P)/nelem]
            imol_endmem = [1]
            # print(nelem)
        else:
            nelem = np.sum(elem_comp,axis=1)
            atom_num = nelem
            # print(nelem)

            # mol_species = np.eye(endmem_num)
            # print(mol_species)
            # mol_endmem = np.dot(mol_species, elem_comp)
            # print(mol_endmem)
            # # chempot = phs.gibbs_energy(T, P, mol=imol,deriv={"dmol":1})
            # # chempot = phs.gibbs_energy(T, P, mol=imol)
            # print(chempot)
            # print(chempot.shape)
            # print('----')
            imol_endmem = []

            for i in iendmem_ids:
                if debug:
                    print(i)
                # imol = np.eye(endmem_num)[i]
                imol_species = np.eye(endmem_num)[i]
                if debug:
                    print(imol_species)
                imol_elems = np.dot(imol_species, elem_comp)
                imol_num = lin.lstsq(phs.props['element_comp'].T,imol_elems)[0]
                imol = np.round(imol_num, decimals=10)
                imol_endmem.append(imol)
                if debug:
                    print(imol)
                # imu += [phs.gibbs_energy(T, P, mol=imol,deriv={"dmol":1})[0,i]/nelem[i]]
                imu += [phs.gibbs_energy(T, P, mol=imol)/nelem[i]]
                if debug:
                    print(imu)
                    # print(nelem[i])
                    print('---')

        endmem_names.extend(iendmem_names)
        endmem_ids.extend(iendmem_ids)
        phs_sym.extend(list(np.tile(abbrev,endmem_num)))
        # print(elem_comp)

        iphs_details['endmem_comp'] = elem_comp
        iphs_details['atom_num'] = atom_num
        iphs_details['endmem_mu'] = np.array(imu)
        iphs_details['endmem_num'] = endmem_num
        iphs_details['endmem_names'] = iendmem_names
        iphs_details['X'] = np.array(imol_endmem)

        phase_comps[phsnm] = elem_comp
        # phase_mu_endmem[phsnm] = np.array(imu)

        mu.extend(imu)
        elem_comps.extend(elem_comp)
        # print(elem_comp)
        # print(phs)
        phase_endmems[phsnm] = iphs_details

    elem_comps = np.vstack(elem_comps)

    natoms = np.sum(elem_comps,axis=1)
    elem_comps = elem_comps/natoms[:,np.newaxis]

    elem_mask = ~np.all(elem_comps<TOL, axis=0)

    for phsnm in phase_comps:
        icomp = phase_comps[phsnm]
        inatom = np.sum(icomp, axis=1)
        inorm_comp = icomp[:,elem_mask]/inatom[:,np.newaxis]
        phase_comps[phsnm] = inorm_comp
        phase_endmems[phsnm]['endmem_comp'] = inorm_comp
        # phase_comps[phsnm] = icomp[:,elem_mask]

    elem_comps = elem_comps[:, elem_mask]
    mu = np.array(mu)
    endmem_names = np.array(endmem_names)
    endmem_ids = np.array(endmem_ids)

    sys_elems = core.chem.PERIODIC_ORDER[elem_mask]

    if prune_polymorphs:
        phs_sym, endmem_ids, mu, elem_comps = (
            remove_polymorphs(phs_sym, endmem_ids, mu, elem_comps))

    phs_sym = np.array(phs_sym)

    # Store initial chempot as all zero reference
    phase_endmems['chempot'] = np.zeros(elem_mask.sum())

    # phase_comps, phase_mu_endmem
    return (phs_sym, endmem_ids, endmem_names, mu, elem_comps,
            sys_elems, phase_endmems)

def remove_polymorphs(phs_sym, endmem_ids, mu, elem_comps, decimals=4):
    elem_round_comps = np.round(elem_comps, decimals=decimals)
        # Drop identical comps
    elem_comps_uniq = np.unique(elem_round_comps, axis=0)

    # uniq_num = elem_comps_uniq.shape[0]
    mu_uniq = []
    phs_sym_uniq = []
    endmem_ids_uniq = []
    for elem_comp in elem_comps_uniq:
        is_equiv_comp = np.all(elem_round_comps == elem_comp[np.newaxis,:], axis=1)
        equiv_ind = np.where(is_equiv_comp)[0]
        min_ind = equiv_ind[np.argsort(mu[equiv_ind])[0]]
        min_mu = mu[min_ind]
        assert np.all(min_mu <= mu[equiv_ind]), 'fail'

        mu_uniq.append(min_mu)
        phs_sym_uniq.append(phs_sym[min_ind])
        endmem_ids_uniq.append(endmem_ids[min_ind])

    mu_uniq = np.array(mu_uniq)
    phs_sym_uniq = np.array(phs_sym_uniq)
    elem_comps_uniq = np.array(elem_comps_uniq)

    return phs_sym_uniq, endmem_ids_uniq, mu_uniq, elem_comps_uniq

def reduce_rank_comp(comp, ignore_oxy=True):
    if ignore_oxy:
        if comp.ndim == 1:
            comp_fit = comp[1:]
        elif comp.ndim == 2:
            comp_fit = comp[:,1:]
        else:
            assert False, ('That is not a valid comp array. ',
                           'It can only have 1 or 2 dimensions.')

    else:
        comp_fit = comp

    return comp_fit

def min_energy_linear_assemblage(bulk_comp, comp, mu, ignore_oxy=True,
                                 TOLmu=10, TOL=1e-5):

    bulk_comp_fit = reduce_rank_comp(bulk_comp, ignore_oxy=ignore_oxy)
    comp_fit = reduce_rank_comp(comp, ignore_oxy=ignore_oxy)

    xy = np.hstack((comp_fit, mu[:,np.newaxis]))
    yavg = np.mean(mu)
    xy_bulk = np.hstack((bulk_comp_fit, yavg))

    wt0, rnorm0 = opt.nnls(xy.T, xy_bulk)
    # print('rnorm',rnorm0)

    def fun(mu, shift=0):
        xy_bulk[-1] = mu
        wt, rnorm = opt.nnls(xy.T, xy_bulk)
        return rnorm-shift


    delmu = .1
    if rnorm0==0:
        shift_dir = -1
        soln_found = True
    else:
        output = opt.minimize_scalar(fun, bounds=[np.min(mu), np.max(mu)])
        xy_bulk[-1] = output['x']
        wt0, rnorm0 = opt.nnls(xy.T, xy_bulk)
        shift_dir = -1

    mu_prev=xy_bulk[-1]
    rnorm=rnorm0

    while True:
        mu_prev = xy_bulk[-1]
        rnorm_prev = rnorm

        xy_bulk[-1] += shift_dir*delmu
        wt, rnorm = opt.nnls(xy.T, xy_bulk)
        delmu *= 2

        # print(shift_dir, rnorm)
        if ((shift_dir==+1)&(rnorm>rnorm_prev)) or ((shift_dir==-1)&(rnorm>0)):
            break


    fun_fit = lambda mu, TOL=TOL: fun(mu, shift=TOL)
    if rnorm > TOL:
        mu_bulk = opt.brentq(fun_fit, mu_prev, xy_bulk[-1], xtol=TOLmu)
        xy_bulk[-1] = mu_bulk
        wt, rnorm = opt.nnls(xy.T, xy_bulk)

    mu_bulk = xy_bulk[-1]
    wt_bulk = wt

    ind_assem = np.where(wt_bulk>0)[0]
    chempot = fit_chempot(ind_assem, bulk_comp_fit, mu,
                          comp_fit, ignore_oxy=ignore_oxy)

    return chempot, mu_bulk, wt_bulk, ind_assem

def calc_phase_affinity_and_comp(iphs, T, P, chempot_endmem,
                                 debug=False, method='special'):

    inatom = iphs.props['atom_num']

    A_formula, iX = iphs.affinity_and_comp(T, P, chempot_endmem, debug=debug, method=method)

    if np.isnan(A_formula):
        ichempot_str = np.array2string(chempot_endmem, separator=', ',max_line_width=1000000)
        print('A NAN value was found by affinity and comp. Try to fix this now...')
        print('phs = modelDB.get_phase(\''+iphs_nm+'\')')
        print("A, X = phs.affinity_and_comp(",T,",",P,", np.array(",ichempot_str,
              "), debug=True, method='special')")
        from IPython import embed; embed()

    inatom_scl = np.dot(inatom,iX)
    A = A_formula/inatom_scl

    return A, iX


def calc_phase_gibbs_surface(iphs, T, P, iX, chempot_endmem=None,
                             verbose=False ):


    ##########
    # UNITS ON CHEMPOT_ENDMEM  ??? basis atomic or not?
    ########
    # print('check basis on chempot_endmem')


    # component_num = iphs.endmember_num
    # comp_species = iphs_endmem['endmem_comp']
    # comp = icomp_species[:icomponent_num]
    # icomp = iphs.props['endmember_comp']


    ## icomp_soln = np.dot(iX, icomp)
    inatom = iphs.props['atom_num']
    inatom_scl = np.dot(inatom,iX)

    if iX.size==1:
        iX = None

    # should be equal to chempot_endmem+A
    mu = np.squeeze(iphs.chem_potential(T,P,mol=iX))
    mu_soln = mu/inatom

    if iX is None:
        iGsoln_mu = mu_soln[0]
        A = iGsoln_mu-chempot_endmem[0]/inatom

        id2Gdm2 = np.array([[0]])
        # Add A calc here...

    else:
        iGsoln_mu = np.dot(mu, iX)/inatom_scl
        iGsoln = iphs.gibbs_energy(T, P, mol=iX)/inatom_scl

        A = iGsoln_mu - np.dot(iX, chempot_endmem)
        # A = A_formula/inatom_scl
        id2Gdm2 = np.squeeze(iphs.gibbs_energy(
            T, P, mol=iX, deriv={'dmol':2}))

        if verbose:
            print('dG = ', iGsoln-iGsoln_mu)

    # if correct_aff:
    #     # switch to ichempot
    #     # iA = iGsoln_mu - np.dot(icomp_soln, chempot)
    #     print(iX)
    #     print(chempot_endmem)
    #     print(np.dot(iX, chempot_endmem))
    #     print(iGsoln_mu)
    #     A_formula = iGsoln_mu - np.dot(iX, chempot_endmem)
    #     A = A_formula/inatom_scl

    if verbose:
        # Should this be A_formula or A????
        iGsoln_aff = A_formula + np.dot(iX, chempot_endmem)
        print('iX', iX)
        print('iGsoln diff = ', iGsoln_aff-iGsoln_mu)

    dmudn_soln = id2Gdm2

    return mu_soln, dmudn_soln, A

def phase_affinity(chempot, phases, phase_endmems,T, P, verbose=False,
                   correct_aff=False, debug=False, method='special'):


    Nphs = len(phases)
    mu_soln = []
    # A = []
    phase_names = []

    A = odict()
    X = odict()
    mu_soln = odict()
    comp_soln = odict()
    natom = odict()
    dmudn_soln = odict()

    ############
    #  TEST
    # iphs_nm = 'Cpx'
    # iphs = phases[iphs_nm]
    # phase_names.append(iphs_nm)
    # iphs_endmem = phase_endmems[iphs_nm]
    # icomponent_num = iphs.endmember_num
    # icomp_species = iphs_endmem['endmem_comp']
    # icomp = icomp_species[:icomponent_num]
    # print(icomp.shape)
    # print(icomp_species.shape)
    # print('chempot = ', chempot.shape)
    # # inatom = iphs_endmem['atom_num']
    # inatom = iphs.props['atom_num']
    # print('inatom = ', inatom.shape)
#
    # ichempot = np.dot(icomp*inatom[:,np.newaxis], chempot)
    # iA, iX = iphs.affinity_and_comp(T, P, ichempot[:icomponent_num], debug=debug, method=method)
    # kernel dies if chempot array has wrong number of elements, give useful error instead
    # from IPython import embed;embed()



    for iphs_nm, iphs in phases.items():
        if verbose:
            print(iphs_nm)
        # print(iphs_nm,iphs)
        # print(iphs)
        phase_names.append(iphs_nm)


        iphs_endmem = phase_endmems[iphs_nm]

        icomponent_num = iphs.endmember_num
        icomp_species = iphs_endmem['endmem_comp']
        icomp = icomp_species[:icomponent_num]


        inatom = iphs.props['atom_num']
        chempot_endmem = np.dot(icomp*inatom[:,np.newaxis], chempot)

        iA, iX = calc_phase_affinity_and_comp(
            iphs, T, P, chempot_endmem, debug=False, method='special')


        imu_soln, idmudn_soln, iA_direct = (
            calc_phase_gibbs_surface(iphs, T, P, iX,
                                     chempot_endmem=chempot_endmem,
                                     verbose=verbose)
        )

        if iX is None:
            icomp_soln = icomp
        else:
            icomp_soln = np.dot(iX, icomp)

        ### inatom = iphs.props['atom_num']

        # icomp = iphs_endmem['endmem_comp']
        # inatom = iphs_endmem['atom_num']

        ### if verbose:
        ###     print('inatom', inatom)
        ###     print('icomp', icomp)
        ###     print('chempot', chempot)

        # icomp *= inatom[:,np.newaxis]
        # print(icomp)
        ###  ichempot = np.dot(icomp*inatom[:,np.newaxis], chempot)
        ###  # print(ichempot)
        ###  # iA, iX = iphs.affinity_and_comp(T, P, ichempot, debug=debug)
        ###  iA, iX = iphs.affinity_and_comp(T, P, ichempot, debug=debug, method=method)
        ###  if np.isnan(iA):
        ###      ichempot_str = np.array2string(ichempot, separator=', ',max_line_width=1000000)
        ###      print('A NAN value was found by affinity and comp. Try to fix this now...')
        ###      print('phs = modelDB.get_phase(\''+iphs_nm+'\')')
        ###      print("A, X = phs.affinity_and_comp(",T,",",P,", np.array(",ichempot_str,
        ###            "), debug=True, method='special')")
        ###      from IPython import embed; embed()

        #icomp /= inatom[:,np.newaxis]
        ### icomp_soln = np.dot(iX, icomp)
        # print('icomp_soln tot = ', icomp_soln.sum())

        X[iphs_nm] = iX
        ### inatom_scl = np.dot(inatom,iX)

        ### if iX.size==1:
        ###     iX = None



        natom[iphs_nm] = inatom
        ### imu = np.squeeze(iphs.chem_potential(T,P,mol=iX))
        # imu /= iphs.props['atom_num']

        # if imu.size==1:
        #     imu = [imu]

        # imu = iphs.gibbs_energy(T,P,mol=iX, deriv={'dmol':1})

        # print(imu)
        ### if iX is None:
        ###     id2Gdm2 = np.array([[0]])
        ### else:
        ###     id2Gdm2 = np.squeeze(iphs.gibbs_energy(
        ###         T, P, mol=iX, deriv={'dmol':2}))
        ###     # print(np.dot(id2Gdm2, iX))
        ###     # idn = 2*np.random.rand(iX.size)-1
        ###     # print(np.dot(id2Gdm2,idn))
        ###     # print('---')


        ### iA_scl = iA/inatom_scl
        ### imu_scl = imu/inatom

        ### if iX is None:
        ###     iGsoln_mu = imu_scl[0]
        ### else:
        ###     iGsoln_mu = np.dot(imu_scl, iX)
        ###     iGsoln = iphs.gibbs_energy(T, P, mol=iX)/inatom_scl
        ###     if verbose:
        ###         print('dG = ', iGsoln-iGsoln_mu)

        ### iGsoln_aff = iA_scl + np.dot(icomp_soln, chempot)

        ### if verbose:
        ###     print('iX', iX)
        ###     print('iGsoln diff = ', iGsoln_aff-iGsoln_mu)

        ### if correct_aff:
        ###     iA = iGsoln_mu - np.dot(icomp_soln, chempot)



        # print(id2Gdm2)
        # scale by natom
        mu_soln[iphs_nm] = imu_soln
        dmudn_soln[iphs_nm] = idmudn_soln

        A[iphs_nm] = iA
        comp_soln[iphs_nm] = icomp_soln
        # print(id2Gdm2.squeeze())


        # print(np.squeeze(imu))
        # print(iX)
        # A.append(iA)
        # mu_soln.extend(imu)

    # A = np.array(A)
    # mu_soln = np.array(mu_soln)
    phase_names = np.array(phase_names)

    return mu_soln, dmudn_soln, comp_soln, X, A, natom,phase_names

def update_phase_affinities(chempot, T, P, phases, phase_endmems,
                            verbose=False, correct_aff=False, debug=False):

    (mu_soln, dmudn_soln, comp_soln_d, X_soln, A_d, natom,
     phase_names_soln) =  phase_affinity(
         chempot, phases, phase_endmems,T, P, verbose=verbose,
         correct_aff=correct_aff, debug=debug)
    extras = {'mu0':mu_soln, 'dmudn':dmudn_soln, 'X':X_soln,
              'elem_comp':comp_soln_d, 'phase_endmems':phase_endmems}

    Aff_soln = np.array(list(A_d.values()))
    comp_soln = np.array(list(comp_soln_d.values()))


    # Aff_endmem = []
    phase_props = phase_endmems.copy()
    chempot_prev = phase_props['chempot']
    dchempot = chempot-chempot_prev
    if verbose:
        print('dchempot = ', dchempot)

    phase_props['chempot'] = chempot

    for iphs in A_d:
        iprops = phase_props[iphs]

        iX_prev = iprops['X']
        iAff_prev = iprops['Aff']
        icomp_prev = iprops['comp']
        imu_prev = iprops['mu']


        # print('---')
        # print(iphs)
        # print('iX = ', iX_prev)
        # print('iAff = ', iAff_prev)
        # print('icomp = ', icomp_prev)
        # print('imu = ', imu_prev)

        if iprops['endmem_num']==1:
            iAff_update = iAff_prev - np.dot(dchempot, icomp_prev.T)
            # print('iAff update (perturb)= ', iAff_update)
            phase_props[iphs]['Aff'] = iAff_update
            continue

        iA_new = A_d[iphs]
        iX_new = X_soln[iphs]
        icomp_new = comp_soln_d[iphs]

        imu_endmem_curr = mu_soln[iphs]
        imu_curr = np.dot(iX_new, imu_endmem_curr)

        imu_new = iA_new + np.dot(chempot, icomp_new)
        if verbose:
            print('mu_curr_error = ', imu_new - imu_curr)

        # imask = phs_sym==iphs
        # iN = iX.size


        # NOTE: this is wrong I think. It needs to use dchempot, not chempot
        # iAff_prev_update = imu_prev - np.dot(chempot, icomp_prev.T)
        # print('iAff update (absolute)= ', iAff_prev_update)

        # Relative adjustment calculation now matches absolute calculation
        # for solution phases
        # assuming that externally initialized properly
        iAff_prev_update = iAff_prev - np.dot(dchempot, icomp_prev.T)
        # print('iAff update (perturb)= ', iAff_prev_update)

        # print('comp_prev = ', icomp_prev)

        iX_update = np.vstack((iX_prev, iX_new))
        iAff_update = np.hstack((iAff_prev_update, iA_new))
        icomp_update = np.vstack((icomp_prev, icomp_new))

        # print('imu_prev = ', imu_prev)
        # print('imu_new = ', imu_new)
        imu_update = np.hstack((imu_prev, imu_new))

        # Aff_endmem.append(np.tile(iA, iN))
        # iendmem_comp = phase_endmems[iphs]['endmem_comp']
        # iendmem_mu = phase_endmems[iphs]['endmem_mu']

        # iA_endmem = iendmem_mu - np.dot(chempot, iendmem_comp.T)

        # phase_props[iphs]['X'] = iX
        # phase_props[iphs]['Aff'] = iA
        # phase_props[iphs]['Aff_endmem'] = iA_endmem
        # phase_props[iphs]['comp'] = ielem_comp

        phase_props[iphs]['X'] = iX_update
        phase_props[iphs]['Aff'] = iAff_update
        phase_props[iphs]['comp'] = icomp_update
        phase_props[iphs]['mu'] = imu_update


        # dmu_endmem[imask] = iA+Ashft

    # Aff_endmem = np.hstack(Aff_endmem)

    # extras['Aff']
    # return phase_props, comp_soln, X_soln, Aff_soln, Aff_endmem, phase_names_soln, extras
    return phase_props, comp_soln, X_soln, Aff_soln, phase_names_soln, extras

def fit_chempot(ind_assem, bulk_comp_fit, mu_compounds,
                comp_compounds_fit, ignore_oxy=True):
    mu_assem = mu_compounds[ind_assem]
    comp_assem_fit = comp_compounds_fit[ind_assem,:]

    output = np.linalg.lstsq(comp_assem_fit, mu_assem, rcond=-1)

    if ignore_oxy:
        chempot = np.hstack((0, output[0]))
    else:
        chempot = output[0]

    return chempot

def init_assem_props(T, P, phs_sym_compound, wt_bulk_compound, comp_compound,
                     phase_comps, phases):

    mu_assem0 = {}
    dmudn_assem0 = {}
    nmol_assem0 = {} #
    endmem_id_assem0 = {} #

    assem_phases = np.unique(phs_sym_compound[wt_bulk_compound>0])
    phs_sym_assem0 = assem_phases.copy() #

    for iphs_sym in assem_phases:
        imask_phs = phs_sym_compound==iphs_sym
        icomp_phs = comp_compound[imask_phs]
        iwt = wt_bulk_compound[imask_phs]
        iphs = phases[iphs_sym]

        # iX = X_soln[iphs_sym]

        iphs_endmem_comps = phase_comps[iphs_sym]

        # assemble mols of total soln phase from endmems and current equil soln comp
        iphs_comp = np.dot(iwt, icomp_phs)
        ioutput = np.linalg.lstsq(iphs_endmem_comps.T, iphs_comp, rcond=-1)
        iendmem_mols = ioutput[0]

        # print(iphs_sym)
        # print('iX', iX)
        # print('comp = ',iphs_comp)
        # print('mol endmem = ', iendmem_mols)
        # print('endmems = ', iphs.endmember_names)
        # print('endmem comps = ', iphs_endmem_comps)
        # print(iphs)

        ielems = iphs.props['element_comp']
        inum_atoms = ielems.sum(axis=1)

        # print('num atoms = ', inum_atoms)

        if iphs.endmember_num > 1:
            ichempot = iphs.chem_potential(T, P, mol=iendmem_mols).squeeze()/inum_atoms
            idmudn = iphs.gibbs_energy(T, P, mol=iendmem_mols, deriv={'dmol':2}).squeeze()
        else:
            ichempot = iphs.gibbs_energy(T, P).squeeze()/inum_atoms
            idmudn = np.array([[0]])





        # print('chempot = ', ichempot)



        # nmol_assem0.extend(iendmem_mols)
        # phs_sym_assem0.extend([iphs_sym for i in range(iphs.endmember_num)])
        # endmem_id_assem0.extend(range(iphs.endmember_num))
        # mu_assem0.extend(list(ichempot))

        nmol_assem0[iphs_sym] = iendmem_mols
        endmem_id_assem0[iphs_sym] = np.arange(iphs.endmember_num)
        mu_assem0[iphs_sym] = ichempot
        dmudn_assem0[iphs_sym] = idmudn

        # print('---')


    # print('=====')
    # print('nmol_assem0 = ', nmol_assem0)
    # print('phs_sym_assem0 = ', phs_sym_assem0)
    # print('endmem_id_assem0 = ', endmem_id_assem0)
    # print('mu_assem0 = ', mu_assem0)
    # print('dmudn_assem0 = ', dmudn_assem0)
    #
    # phs_sym_assem0
    assem_props = {}

    assem_props['nmol'] = nmol_assem0
    assem_props['phs_sym'] = phs_sym_assem0
    assem_props['endmem_id'] = endmem_id_assem0
    assem_props['mu0'] = mu_assem0
    assem_props['dmudn0'] = dmudn_assem0

    return assem_props

def get_linear_phase_model(dmudn, X, mu0):

    # M_tot = np.zeros((Nendmem, Nendmem))
    # dndmu_tot = np.zeros((Nendmem, Nendmem))
    dmudn_tot = np.zeros((Nendmem, Nendmem))
    nmol_end0 = np.zeros(Nendmem)
    mu0_endmem = np.zeros(Nendmem)

    ind_endmem = 0
    for phsnm in mu0:
        idmudn = dmudn[phsnm]
        iX = X[phsnm]
        iendmem_num = iX.size

        imu0 = mu0[phsnm]
        # icomp = comp[phsnm]
        # iphs_comp = phase_comps[phsnm]

        # idndmu = np.linalg.pinv(idmudn)
        # iM = np.dot(idmudn, idndmu)
        #
        # M_tot[ind_endmem:ind_endmem+iendmem_num,
        #       ind_endmem:ind_endmem+iendmem_num] = iM

        # dndmu_tot[ind_endmem:ind_endmem+iendmem_num,
        #           ind_endmem:ind_endmem+iendmem_num] = idndmu
        dmudn_tot[ind_endmem:ind_endmem+iendmem_num,
                  ind_endmem:ind_endmem+iendmem_num] = idmudn

        nmol_end0[ind_endmem:ind_endmem+iendmem_num] = iX

        mu0_endmem[ind_endmem:ind_endmem+iendmem_num] = imu0

        ind_endmem += iendmem_num

    return mu0_endmem, nmol_end0, dmudn_tot

def refine_assemblage(bulk_comp, chempot_init, mu0, dmudn, X):

    mu0_endmem, nmol_end0, dmudn_tot  = get_linear_phase_model(dmudn, X, mu0)

    A, B = get_linear_constraints(bulk_comp, Gtot_init, mu0_endmem,
                                  nmol_end0, dmudn_tot,
                                  phase_endmem_num, comp_endmem)

    B['Gtot'] = Gtot_init-Gshft


    A_lsq, B_lsq = build_lstsq(A, B, Affwt_endmem, phase_endmem_num,
                               sig_nmol_prior=sig_nmol_prior,
                               sig_comp=sig_comp, sig_mu=sig_mu)

    output = np.linalg.lstsq(A_lsq, B_lsq, rcond=None)
    params = output[0]
    chempot_fit = params[:Nelems]
    nmol_fit = params[Nelems:]

def discover_approx_hull(bulk_comp, chempot_init, Gtot_init, Aff_phases_init,
                         mu0, dmudn, X, phase_endmem_num, comp_endmem,
                         debug=False, muTOL=0.1, sig_nmol_prior=.2,
                         sig_comp=.01, sig_mu=10, Ascl_min=1, Ascl_max=1e3,
                         Nsteps=10, max_iter=50, Gshft=1e3):

    Aff_phases = Aff_phases_init
    Npure = np.sum(phase_endmem_num==1)

    # Gtot_init = np.dot(chempot_init, bulk_comp)

    mu0_endmem, nmol_end0, dmudn_tot  = get_linear_phase_model(dmudn, X, mu0)


    A, B = get_linear_constraints(bulk_comp, Gtot_init, mu0_endmem,
                                  nmol_end0, dmudn_tot,
                                  phase_endmem_num, comp_endmem)

    B['Gtot'] = Gtot_init-Gshft

    Ascl_thresh = Ascl_max
    nmol = nmol_end0.copy()
    chempot = chempot_init.copy()


    Ascl_fac = np.exp(np.log(Ascl_max/Ascl_min)/Nsteps)


    count=1
    while True:

        # Affwt_endmem, Affwt = get_affinity_wt(Aff_phases, phase_endmem_num, Ascl_thresh)
        Affwt_endmem, Affwt = get_affinity_wt(Aff_phases_init, phase_endmem_num, Ascl_thresh)
        A_lsq, B_lsq = build_lstsq(A, B, Affwt_endmem, phase_endmem_num,
                                   sig_nmol_prior=sig_nmol_prior,
                                   sig_comp=sig_comp, sig_mu=sig_mu)

        output = np.linalg.lstsq(A_lsq, B_lsq, rcond=None)
        params = output[0]
        chempot_fit = params[:Nelems]
        nmol_fit = params[Nelems:]
        Aff_phases_fit = update_fitted_affinities(
            params, A, B, phase_endmem_num)
        dnmol = nmol_fit-nmol
        dchempot = chempot_fit-chempot

        resid = np.dot(A_lsq, params)-B_lsq
        chisqr = np.sqrt(np.mean(resid**2))


        if debug:
            print('chi2 = ', chisqr)
            print('Aff_phases_fit = ', Aff_phases_fit)
            print('sig. wt count = ', np.sum(Affwt_endmem>.05))
            print('Ascl_thresh = ', Ascl_thresh)
            print(np.max(dnmol))
            print('    dchempot = ',np.max(np.abs(dchempot)))
            print('----------')

        # if np.all(np.abs(dnmol) < TOL):
        #     break

        if count >= max_iter:
            break

        if np.all(np.abs(dchempot) < muTOL):
            break

        Aff_phases = Aff_phases_fit
        nmol = nmol_fit
        chempot = chempot_fit

        if Ascl_thresh > Ascl_min:
            Ascl_thresh /= Ascl_fac

        count += 1

    return chempot_fit, nmol_fit, Aff_phases_fit, Affwt
