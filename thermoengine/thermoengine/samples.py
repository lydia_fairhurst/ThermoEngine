from __future__ import annotations  # Enable Python 4 type hints in Python 3
import abc
import collections
from functools import wraps
from typing import Type, List, Optional
from typing import NewType, TypeVar

import numpy as np
from numpy.typing import NDArray
import thermoengine.phases
from scipy import optimize as opt

from thermoengine import phases as phs
from thermoengine import chem, chemistry
from thermoengine import Phase

from thermoengine.core import UnorderedList

from dataclasses import dataclass

__all__ = ['SampleMaker']

# Phase = NewType('Phase', thermoengine.phases.Phase)

class _PhaseSample(abc.ABC):
    phase_model: phs.Phase
    X: np.ndarray
    T: float
    P: float
    fixed_comp: bool


    comp: chemistry.ElemComp
    G: float
    amount: float
    atom_num: int
    name: str
    endmember_num: int

    def __init__(self, phase_model: phs.Phase,
                 X: List[float] = None, T: float = 1000,
                 P: float = 1, amount: float = 1):
        self._init_phase_model(phase_model)
        self._init_conditions(T, P, X)
        self.G = self.gibbs_energy()
        self.amount = amount

    def _init_conditions(self, T, P, X):
        self.T = T
        self.P = P
        if X is None:
            self._X = np.eye(self.phase_model.endmember_num)[0]
        else:
            X = np.array(X)
            assert X.size == self.endmember_num, \
                'dimensions of X do not match number of endmembers.'
            self._X = X
        self.comp = self.get_molar_elem_comp()

    @property
    def phase_model(self) -> phs.Phase:
        return self._phase_model

    def _init_phase_model(self, model):
        self._phase_model = model
        self.is_solution = model.phase_type == 'solution'
        self.fixed_comp = not self.is_solution
        self.name = model.abbrev
        self.endmember_num = model.endmember_num
        atom_num = model.props['atom_num']
        if not np.isscalar(atom_num):
            atom_num = atom_num[0]
        self.atom_num = atom_num

    @property
    def X(self):
        return self._X


    def get_molar_elem_comp(self):
        elem_comp = self.phase_model.props['element_comp']
        elem_mask = np.any(elem_comp > 0, axis=0)
        mol_elems = self.X.dot(elem_comp[:, elem_mask])
        elems = chem.PERIODIC_ORDER[elem_mask]
        return  chemistry.ElemComp(**dict(zip(elems, mol_elems)))

    def modify_sample_conditions(self, T=None, P=None, X=None, amount=None):
        if T is None:
            T = self.T
        if P is None:
            P = self.P
        if X is None:
            X = self.X
        if amount is None:
            amount = self.amount

        return SampleMaker.get_sample(self.phase_model, X=X, T=T, P=P, amount=amount)

    def optionalX(func):
        @wraps(func)
        def check_input(*args, **kwargs):
            self = args[0]
            X = kwargs['X'] if ('X' in kwargs) else None
            if X is None:
                X = self.X

            result = func(self, X = X)
            return result
        return check_input


    @optionalX
    def gibbs_energy(self, X: Optional[np.ndarray]=None):
        return self._calc_gibbs_energy(X)

    @optionalX
    def chem_potential(self, X=None):
        return self._calc_chem_potential(X)

    @abc.abstractmethod
    def _calc_gibbs_energy(self, X):
        pass

    @abc.abstractmethod
    def _calc_chem_potential(self, X):
        pass




    ## variable_comp
    def equilibrate_by_chemical_exchange(self, chem_potential):
        self.chem_potential_ref = chem_potential
        self.Xref = self.X
        result = opt.minimize(self.affinity_perturbation,
                              x0=np.zeros(self.X.size))

        return SampleMaker.get_sample(
            self.phase_model, X=self._calc_perturbed_comp(result.x),
            T=self.T, P=self.P)

    def _calc_perturbed_comp(self, dlogX):
        X = np.exp(dlogX) * self.Xref
        return X / X.sum()

    def affinity_perturbation(self, dlogX):
        X = self._calc_perturbed_comp(dlogX)
        return self.affinity(X)

    def affinity(self, X):
        dmu = self.chem_potential(X=X) - self.chem_potential_ref
        return np.dot(dmu, X)

    def __eq__(self, other):
        same_model = self.phase_model == other.phase_model
        same_fixed_comp = self.fixed_comp == other.fixed_comp
        same_model_config = same_model and same_fixed_comp

        same_T = self.T == other.T
        same_P = self.P == other.P
        same_X = np.all(self.X == other.X)
        same_conditions =  same_T and same_P and same_X

        return same_model_config and same_conditions

    def __lt__(self, other):
        return self.name < other.name

class SampleMaker:
    XTOL = 1e-8

    @classmethod
    def get_sample(cls, phase_model: Phase,
                   X: List[float] = None, T: float = 1000,
                   P: float = 1, amount: float = 1) -> _PhaseSample:
        """
        Create PhaseSample with given composition and environmental conditions.

        Parameters
        ----------
        phase_model
            Phase model providing gibbs surface.
        X
            Molar composition of phase in terms of endmember components.
        T
            Temperature in Kelvin.
        P
            Pressure in bars.

        Returns
        -------
        PhaseSample
            Phase Sample with desired composition and environmental variables.

        """

        if phase_model.phase_type == 'solution':
            return _SolutionPhaseSample(phase_model=phase_model, X=X, T=T, P=P, amount=amount)
        else:
            return _PurePhaseSample(phase_model=phase_model, X=X, T=T, P=P, amount=amount)

    @classmethod
    def get_fixed_comp_sample(cls, phase_model: phs.Phase,
                              X: List[float] = None, T: float = 1000,
                              P: float = 1, amount: float = 1) -> _PhaseSample:

        if phase_model.phase_type == 'solution':
            return _FixedCompPhaseSample(phase_model=phase_model, X=X, T=T, P=P, amount=amount)
        else:
            return _PurePhaseSample(phase_model=phase_model, X=X, T=T, P=P, amount=amount)

    @classmethod
    def get_sample_endmembers(cls, phase_model: phs.Phase, T: float = 1000,
                              P: float = 1, amount: float = 1) -> List[_PhaseSample]:

        X_endmems = cls._get_phase_endmember_comps(phase_model)
        return cls._get_sample_set_from_X_grid(phase_model, X_endmems, T, P, amount)

    @classmethod
    def get_sample_grid(cls, phase_model: phs.Phase, grid_spacing: float = 0.2,
                        T: float = 1000, P: float = 1, amount: float = 1) -> List[_PhaseSample]:

        Nendmem = phase_model.endmember_num
        Xgrid = cls.build_comp_grid(Nendmem, grid_spacing)
        return cls._get_fixed_sample_set_from_X_grid(phase_model, Xgrid, T, P, amount)

    @classmethod
    def _get_fixed_sample_set_from_X_grid(cls, phase_model, Xgrid, T, P, amount) -> List[_PhaseSample]:
        sample_set = []
        [sample_set.append(SampleMaker.get_fixed_comp_sample(
            phase_model, X=X, T=T, P=P, amount=amount))
         for X in Xgrid]
        return sample_set

    @classmethod
    def _get_sample_set_from_X_grid(cls, phase_model, Xgrid, T, P, amount):
        sample_set = []
        [sample_set.append(SampleMaker.get_sample(phase_model, X=X, T=T, P=P, amount=amount))
         for X in Xgrid]
        return sample_set


    @classmethod
    def build_comp_grid(cls, Nendmem, grid_spacing, rel_tol=1e-2):
        if Nendmem == 1:
            return np.array([1])

        iXgrid = cls._build_endmem_grid(grid_spacing)
        Xcoor_independent = cls._build_mesh_of_independent_endmems(Nendmem, iXgrid)
        Xcoor_independent_norm = cls._filter_results_by_normalization_constraint(Xcoor_independent)
        Xcoor = cls._append_final_normalized_endmem(Xcoor_independent_norm)
        return Xcoor

    @classmethod
    def _append_final_normalized_endmem(cls, Xcoor_independent_norm):
        Xcoor_sum = Xcoor_independent_norm.sum(axis=1)
        Xcoor = np.hstack((Xcoor_independent_norm, 1 - Xcoor_sum[:, np.newaxis]))
        return Xcoor

    @classmethod
    def _filter_results_by_normalization_constraint(cls, Xcoor_independent):
        Xcoor_sum = Xcoor_independent.sum(axis=1)
        Xcoor_independent_norm = Xcoor_independent[Xcoor_sum <= 1]
        return Xcoor_independent_norm

    @classmethod
    def _build_mesh_of_independent_endmems(cls, Nendmem, iXgrid):
        Xgrid_set = [iXgrid for n in range(Nendmem)]
        Xi_values = np.meshgrid(*Xgrid_set[:-1])
        Xcoor_independent = np.vstack([Xi.ravel() for Xi in Xi_values])
        return Xcoor_independent.T

    @classmethod
    def _build_endmem_grid(cls, grid_spacing):
        inv_spacing = 1 / grid_spacing
        division_num = int(np.round(inv_spacing))
        iXgrid = np.linspace(0, 1, division_num + 1)
        return iXgrid

    @classmethod
    def _get_phase_endmember_comps(cls, phs):
        endmem_num = phs.endmember_num
        X_endmems = np.eye(endmem_num) + cls.XTOL
        return X_endmems


class _PurePhaseSample(_PhaseSample):
    def _calc_gibbs_energy(self, X):
        return self.phase_model.gibbs_energy(self.T, self.P) / self.atom_num

    def _calc_chem_potential(self, X):
        return self.phase_model.chem_potential(self.T, self.P) / self.atom_num

class _SolutionPhaseSample(_PhaseSample):
    def _calc_gibbs_energy(self, X):
       return self.phase_model.gibbs_energy(self.T, self.P, mol=X) / self.atom_num

    def _calc_chem_potential(self, X):
        return self.phase_model.chem_potential(self.T, self.P, mol=X) / self.atom_num

class _FixedCompPhaseSample(_SolutionPhaseSample):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fixed_comp = True


class Assemblage(collections.MutableSequence):
    """
    Stores state of coexisting phase samples

    Provides enhanced list for PhaseSample objects that form a
    coexisting phase assemblage. Provides attributes that track
    the state of each phase sample.

    Attributes
    ----------
    samples : List[PhaseSample]
        list of phase samples present in assemblage

    total_energy : float
        total energy of phase assemblage given amounts/comp of each phase
    total_comp : chemistry.Comp
        total composition of assemblage

    sample_energies : NDArray[float]
        list of energies for each sample in assemblage
    sample_amounts : NDArray[float]
        list of molar amounts of each sample
    sample_endmem_comps : NDArray[float]
        list of endmember compositions (X-values) for each sample
    sample_names : NDArray[str]
        list of sample names

    """
    samples: Optional[List[_PhaseSample]]

    def __init__(self, samples: Optional[List[_PhaseSample]] = None):
        """
        Create new assemblage with desired phase samples

        Parameters
        ----------
        samples :
            list of phase samples for inclusion in assemblage
        """
        if samples is None:
            samples = []

        self.samples = samples

    @property
    def total_energy(self) -> float:
        return np.dot(self.sample_amounts, self.sample_energies)

    @property
    def total_comp(self) -> chemistry.Comp:
        return sum([samp.comp for samp in self.samples])

    @property
    def sample_energies(self) -> NDArray[float]:
        return np.array([samp.G for samp in self.samples])

    @property
    def sample_amounts(self) -> NDArray[float]:
        return np.array([samp.amount for samp in self.samples])

    @property
    def sample_endmem_comps(self) -> NDArray[NDArray[float]]:
        """Return array of molar endmember compositions for every sample"""
        return np.array([samp.X for samp in self.samples])

    @property
    def sample_names(self) -> NDArray[str]:
        return np.array([samp.name for samp in self.samples])

    @property
    def unique_phase_names(self) -> NDArray[str]:
        return np.unique(self.sample_names)

    @property
    def multi_sample_phase_names(self) -> NDArray[str]:
        """
        sample phase names for phases that appear multiple times in assemblage
        """
        phase_sample_count = np.array([np.sum(self.sample_names == name)
                                       for name in self.unique_phase_names])
        return self.unique_phase_names[phase_sample_count > 1]

    @property
    def pure_samples(self):
        """list of only pure samples from assemblage"""
        return [samp for samp in self.samples if samp.endmember_num == 1]

    @property
    def solution_samples(self):
        """list of only solution samples from assemblage"""
        return [samp for samp in self.samples if samp.endmember_num > 1]

    def remove_redundant_endmembers(self) -> Assemblage:
        """filter out redundant pure samples represented as solution endmembers"""
        endmember_names = self._get_solution_endmember_names()
        filtered_samples = self._get_only_non_endmember_pure_samples(endmember_names)
        filtered_samples += self.solution_samples
        return Assemblage(filtered_samples)

    def _get_only_non_endmember_pure_samples(self, endmember_names):
        filtered_samples = []
        for pure_samp in self.pure_samples:
            pure_name = pure_samp.phase_model.phase_name.lower()
            if pure_name not in endmember_names:
                filtered_samples.append(pure_samp)
        return filtered_samples

    def _get_solution_endmember_names(self) -> List[_PhaseSample]:
        endmember_names = []
        for samp in self.solution_samples:
            endmember_names.extend([
                nm.lower() for nm in samp.phase_model.endmember_names])
        return endmember_names

    def get_subset_for_phase(self, phase_name : str) -> MonophaseAssemblage:
        """
        Get assemblage of all samples for desired phase

        Parameters
        ----------
        phase_name :
            Desired phase name

        Returns
        -------
        Assemblage of desired phase samples
        """
        mask = [name == phase_name for name in self.sample_names]
        ind = np.where(mask)[0]
        return MonophaseAssemblage([self.samples[i] for i in ind])

    def __eq__(self, other):
        if other is None:
            other = Assemblage()

        return sorted(self.samples) == sorted(other.samples)


    def __getitem__(self, key):
        ''' retrieves an item by its index, key'''
        return self.samples[key]

    def __setitem__(self, key, value):
        ''' set the item at index, key, to value '''
        self.samples[key] = value

    def __delitem__(self, key):
        ''' removes the item at index, key '''
        del self.samples[key]

    def __len__(self):
        return len(self.samples)

    def insert(self, key, value):
        ''' add an item, value, at index, key. '''
        self.samples.insert(key, value)

class MonophaseAssemblage(Assemblage):
    """Special case of an assemblage with all coexisting samples from same phase"""

    def all_samples_resolved(self, resolution : float, rel_tol : float =1e-2):
        """returns whether all samples are resolved relative to given resolution for a single phase assemblage"""
        sample_pairs_resolved = self._sample_pairs_resolved(resolution, rel_tol)
        uniq_sample_pairs_resolved = self._get_unique_pairs(sample_pairs_resolved)
        return np.all(uniq_sample_pairs_resolved)

    def segregate_resolved_samples(self, resolution : float,
                                   rel_tol : float = 1e-2) -> List[MonophaseAssemblage]:
        """
        Split monophase assemblage into a list of resolved sample clusters

        Parameters
        ----------
        resolution
            desired molar resolution defining indistinguishable sample clusters
        rel_tol
            relative tolerance on sample resolution

        Returns
        -------
        assem_groups

        """
        sample_pairs_resolved = self._sample_pairs_resolved(resolution, rel_tol)

        if self._all_samples_resolved(sample_pairs_resolved):
            return [MonophaseAssemblage([samp]) for samp in self.samples]

        assem_pool = MonophaseAssemblage(self.samples)

        assem_groups = []
        while(len(assem_pool)>0):
            sample_cluster = self._extract_sample_cluster_from_pool(assem_pool, resolution, rel_tol)
            assem_groups.append(sample_cluster)

        return assem_groups

    def get_mixed_subset_for_phase(self) -> MonophaseAssemblage:
        """
        Mix  all samples for desired phase and get as single phase assemblage

        Returns
        -------
        single phase Assemblage obtained by mixing selected phase samples
        """
        total_amount = self.sample_amounts.sum()

        sample_frac = self.sample_amounts / total_amount
        X_avg = np.dot(sample_frac, self.sample_endmem_comps)

        samp = self.samples[0]
        mixed_assem = MonophaseAssemblage([samp.modify_sample_conditions(
            X=X_avg, amount=total_amount)])
        return mixed_assem

    def _all_samples_resolved(self, sample_pairs_resolved) -> bool:
        return np.all(self._get_unique_pairs(sample_pairs_resolved))

    def _sample_pairs_resolved(self, resolution : float, rel_tol : float):
        Xsamps = self.sample_endmem_comps
        dX_pairs = np.swapaxes(Xsamps[:, :, np.newaxis] - Xsamps.T, 1, 2)
        axis_resolved = np.abs(dX_pairs) > resolution * (1 + rel_tol)
        sample_pairs_resolved = np.any(axis_resolved, axis=2)
        return sample_pairs_resolved

    def _get_unique_pairs(self, sample_pairs_resolved):
        ind_pairs = np.triu_indices(n=len(self.samples), k=1)
        uniq_sample_pairs_resolved = sample_pairs_resolved[ind_pairs]
        return uniq_sample_pairs_resolved

    def _extract_sample_cluster_from_pool(self, assem_pool: MonophaseAssemblage,
                                          resolution: float, rel_tol: float):
        resolved_pairs_in_pool = assem_pool._sample_pairs_resolved(
            resolution, rel_tol)
        in_cluster = ~resolved_pairs_in_pool[0]
        ind_cluster = np.where(in_cluster)[0]
        cluster_samples = [assem_pool[ind] for ind in ind_cluster]
        [assem_pool.remove(isamp) for isamp in cluster_samples]
        return MonophaseAssemblage(cluster_samples)