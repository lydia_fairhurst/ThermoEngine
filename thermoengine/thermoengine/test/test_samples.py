import numpy as np

import thermoengine as thermo
from thermoengine import samples as smp
from thermoengine.samples import SampleMaker, Assemblage, MonophaseAssemblage

from thermoengine import UnorderedList
from thermoengine.test_utils import are_close, all_arrays_are_close

modelDB = thermo.model.Database()
XTOL = 1e-5


class TestPhaseSample:
    def test___should_get_sample_with_sample_maker(self):
        Qz, Cpx, Fsp = modelDB.get_phases(['Qz', 'Cpx', 'Fsp'])

        smp._PurePhaseSample(Qz)
        assert (smp.SampleMaker.get_sample(Qz) ==
                smp._PurePhaseSample(Qz))
        assert (smp.SampleMaker.get_sample(Cpx) ==
                smp._SolutionPhaseSample(Cpx))
        assert (smp.SampleMaker.get_fixed_comp_sample(Fsp) ==
                smp._FixedCompPhaseSample(Fsp))

    def test_should_return_pure_phase_sample_properties_using_atomic_basis(self):
        Qz = modelDB.get_phase('Qz')
        atom_num = 3
        qz_samp = smp.SampleMaker.get_sample(Qz)
        assert qz_samp.G == Qz.gibbs_energy(qz_samp.T, qz_samp.P) / atom_num

    def test_should_return_solution_phase_sample_properties_using_atomic_basis(self):
        Fsp = modelDB.get_phase('Fsp')
        atom_num = 13
        fsp_samp = smp.SampleMaker.get_sample(Fsp, X=[0.5, 0.5, 0])
        assert fsp_samp.G == Fsp.gibbs_energy(fsp_samp.T, fsp_samp.P, mol=fsp_samp.X) / atom_num
        assert are_close(
            fsp_samp.chem_potential(),
            Fsp.chem_potential(fsp_samp.T, fsp_samp.P, mol=fsp_samp.X) / atom_num)

    def test_should_store_fixed_comp(self):
        qz_samp = smp.SampleMaker.get_sample(modelDB.get_phase('Qz'))
        fsp_samp = smp.SampleMaker.get_sample(modelDB.get_phase('Fsp'))
        fixed_fsp_samp = smp.SampleMaker.get_fixed_comp_sample(modelDB.get_phase('Fsp'))

        assert qz_samp.fixed_comp
        assert not fsp_samp.fixed_comp
        assert fixed_fsp_samp.fixed_comp

    def test_should_differentiate_solution_samples_and_fixed_comp_samples(self):
        Fsp = modelDB.get_phase('Fsp')
        fsp_samp = smp.SampleMaker.get_sample(Fsp)
        fixed_fsp_samp = smp.SampleMaker.get_fixed_comp_sample(Fsp)

        assert not fixed_fsp_samp == fsp_samp

class TestSampleComps:
    def test___should_compare_samples_for_equivalence(self):
        fsp = modelDB.get_phase('Fsp')
        kspar_samp = smp.SampleMaker.get_sample(fsp, X=[0.5, 0, 0.5])

        assert kspar_samp == smp.SampleMaker.get_sample(fsp, X=[0.5, 0, 0.5])
        assert not kspar_samp == smp.SampleMaker.get_sample(fsp, X=[0.5, 0, 0.5], P = 1e3)
        assert not kspar_samp == smp.SampleMaker.get_sample(fsp, X=[0.75, 0, 0.25])

    def test_should_store_individual_elemental_composition_of_solution_phases_sample(self):
        fsp = modelDB.get_phase('Fsp')
        kspar_samp = smp.SampleMaker.get_sample(fsp, X=[0.5, 0, 0.5])

        assert kspar_samp.comp == thermo.ElemComp(Na=0.5, Ca=0, K=0.5, Al=1, Si=3, O=8)

    def test___should_store_compositions_for_set_of_solution_phase_samples(self):
        fsp = modelDB.get_phase('Fsp')
        fsp_samp = [smp.SampleMaker.get_sample(fsp, X=[1, 0, 0]),
                    smp.SampleMaker.get_sample(fsp, X=[0, 1, 0]),
                    smp.SampleMaker.get_sample(fsp, X=[0, 0, 1])]
        fsp_samp_comp = UnorderedList([samp.comp for samp in fsp_samp])
        assert fsp_samp_comp == [
            thermo.ElemComp(K=0, Na=1, Ca=0, Al=1, Si=3, O=8),
            thermo.ElemComp(K=0, Na=0, Ca=1, Al=2, Si=2, O=8),
            thermo.ElemComp(K=1, Na=0, Ca=0, Al=1, Si=3, O=8)]

    def test_should_get_samples_with_endmember_comp_for_pure_phase(self):
        Qz_samps = smp.SampleMaker.get_sample_endmembers(modelDB.get_phase('Qz'))
        assert len(Qz_samps) == 1
        assert are_close([samp.X for samp in Qz_samps], [1], abs_tol=XTOL)

    def test_should_get_samples_with_endmember_comps_for_solution_phase(self):
        Fsp_samps = smp.SampleMaker.get_sample_endmembers(modelDB.get_phase('Fsp'))
        X_samps = np.vstack([samp.X for samp in Fsp_samps])
        assert len(Fsp_samps) == 3
        assert are_close(X_samps, [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                         abs_tol=XTOL)

    def test_should_get_sample_grid_over_compositional_binary(self):
        assert len(smp.SampleMaker.get_sample_grid(
            modelDB.get_phase('Bt'), grid_spacing=1/5)) == 6

    def test_should_get_sample_grid_over_compositional_ternary(self):
        ternary_samps = smp.SampleMaker.get_sample_grid(
            modelDB.get_phase('Fsp'), grid_spacing=1/4)
        assert len(ternary_samps) == 15
        assert np.unique([samp.X for samp in ternary_samps], axis=0).shape[0] == 15


    def test___should_build_single_sample_comp_grid_for_pure_phase(self):
        pure_comp_grid = smp.SampleMaker.build_comp_grid(1, 1)
        assert len(pure_comp_grid) == 1
        assert pure_comp_grid == 1


    def test___should_build_binary_composition_grid_with_desired_spacing(self):
        binary_grid = smp.SampleMaker.build_comp_grid(2, 0.1)
        X0 = binary_grid[:, 0]
        assert are_close(np.diff(X0), 0.1)

    def test___should_build_ternary_composition_grid_with_desired_spacing(self):
        ternary_grid = smp.SampleMaker.build_comp_grid(3, 0.2)
        endmember_grids = [np.unique(Xi) for Xi in ternary_grid.T]
        assert are_close(np.diff(endmember_grids), 0.2)

    def test___should_build_Ndimensional_composition_grid_with_desired_spacing(self):
        ND_grid = smp.SampleMaker.build_comp_grid(5, 1/3)
        endmember_grids = [np.unique(Xi) for Xi in ND_grid.T]
        assert are_close(np.diff(endmember_grids), 1/3)

    def test___should_build_composition_grid_with_normalized_comps(self):
        ternary_grid = smp.SampleMaker.build_comp_grid(3, 0.2)
        assert np.all(ternary_grid.sum(axis=1) == 1)

        ND_grid = smp.SampleMaker.build_comp_grid(5, 1 / 4)
        assert np.all(ND_grid.sum(axis=1) == 1)

    def test___should_build_ternary_grid_with_desired_resolution(self):
        ternary_grid_1 = smp.SampleMaker.build_comp_grid(3, 1)
        assert len(ternary_grid_1) == 3

        ternary_grid_2 = smp.SampleMaker.build_comp_grid(3, 1/2)
        assert len(ternary_grid_2)== 6

        ternary_grid_3 = smp.SampleMaker.build_comp_grid(3, 1/3)
        assert len(ternary_grid_3) == 10

    def test___should_build_quaternary_grid_with_desired_resolution(self):
        ternary_grid_1 = smp.SampleMaker.build_comp_grid(4, 1)
        assert len(ternary_grid_1) == 4

        ternary_grid_2 = smp.SampleMaker.build_comp_grid(4, 1/2)
        assert len(ternary_grid_2)== 10

class TestAssemblage:
    def test_should_create_assemblage_from_list_of_samples(self):
        samples = [smp.SampleMaker.get_sample(phs)
                   for phs in modelDB.get_phases(['Qz','Ol','Cpx','Fsp'])]
        assem = smp.Assemblage(samples)

        assert assem == smp.Assemblage(samples)
        assert not assem == smp.Assemblage()

    def test_should_retrieve_sample_properties_from_assemblage(self):

        samples = [SampleMaker.get_sample(phs)
                   for phs in modelDB.get_phases(['Qz', 'Ol', 'Cpx', 'Fsp'])]
        assem = smp.Assemblage(samples)


        assert are_close(assem.sample_energies,
                         [-327577.07980718615, -280701.47311117913,
                          -344672.3421547144, -328799.95127329574])

        assert are_close(assem.sample_amounts, [1, 1, 1, 1])

        assert all_arrays_are_close(assem.sample_endmem_comps,
                                    [np.array([1.]),
                                     np.array([1., 0., 0., 0., 0., 0.]),
                                     np.array([1., 0., 0., 0., 0., 0., 0.]),
                                     np.array([1., 0., 0.])])

    def test_should_calculate_total_comp_of_samples(self):
        samples = [SampleMaker.get_sample(phs)
                   for phs in modelDB.get_phases(['Qz', 'Fo'])]
        assem = smp.Assemblage(samples)

        assert assem.total_comp == thermo.OxideComp(MgO=2, SiO2=1+1)
        assert not assem.total_comp == thermo.OxideComp(SiO2=1)

    def test_should_get_samples_by_phase(self):
        Fsp = modelDB.get_phase('Fsp')
        Qz = modelDB.get_phase('Qz')
        Qz_sample = SampleMaker.get_sample(Qz)
        Fsp_samples = [SampleMaker.get_sample(Fsp, X=[0,1,0]),
                       SampleMaker.get_sample(Fsp, X=[0,0,1])]
        assem = Assemblage(Fsp_samples+[Qz_sample])

        assert assem.get_subset_for_phase('Fsp') == Assemblage(Fsp_samples)
        assert assem.get_subset_for_phase('Qz') == Assemblage([Qz_sample])

    def test_should_filter_duplicate_endmember_phases(self):
        Fsp = modelDB.get_phase('Fsp')
        Fsp_samples = SampleMaker.get_sample_endmembers(Fsp)

        endmembers = modelDB.get_phases(['Ab','An','Sa'])
        endmem_samples = [SampleMaker.get_sample(phs) for phs in endmembers]

        assem = Assemblage(Fsp_samples+endmem_samples)
        assem_filtered = assem.remove_redundant_endmembers()

        for endmem in endmem_samples:
            assert endmem not in assem_filtered

        assert assem_filtered == Assemblage(Fsp_samples)

    def test_should_segregate_resolved_samples_into_separate_assemblages(self):
        res = 0.1
        Fsp = modelDB.get_phase('Fsp')
        resolved_samples = [
            SampleMaker.get_sample(Fsp, X=[1, 0, 0]),
            SampleMaker.get_sample(Fsp, X=[0, 1, 0]),
            SampleMaker.get_sample(Fsp, X=[0, 0, 1])]
        resolved_assem_groups = (
            MonophaseAssemblage(resolved_samples).segregate_resolved_samples(res))
        assert len(resolved_assem_groups) == 3

    def test_should_group_unresolved_samples_into_single_assemblage(self):
        res = 0.1
        Fsp = modelDB.get_phase('Fsp')
        unresolved_samples = [
            SampleMaker.get_sample(Fsp, X=[1, 0, 0]),
            SampleMaker.get_sample(Fsp, X=[.95, .05, 0]),
            SampleMaker.get_sample(Fsp, X=[.95, 0, .05])]
        unresolved_assem_groups = (
            MonophaseAssemblage(unresolved_samples).segregate_resolved_samples(res))
        assert len(unresolved_assem_groups) == 1

    def test_should_separate_partly_resolved_samples_into_diff_assemblages(self):
        res = 0.1
        Fsp = modelDB.get_phase('Fsp')
        partly_resolved_samples = [
            SampleMaker.get_sample(Fsp, X=[.5, .5, 0]),
            SampleMaker.get_sample(Fsp, X=[.55, .45, 0]),
            SampleMaker.get_sample(Fsp, X=[0, 0, 1])]
        partly_resolved_assem_groups = (
            MonophaseAssemblage(partly_resolved_samples).segregate_resolved_samples(res))
        assert len(partly_resolved_assem_groups) == 2

    def test_should_segregate_unresolved_sample_clusters_into_diff_assemblages(self):
        res = 0.1
        Fsp = modelDB.get_phase('Fsp')
        unresolved_clustered_samples = [
            SampleMaker.get_sample(Fsp, X=[.5, .5, 0]),
            SampleMaker.get_sample(Fsp, X=[.55, .45, 0]),
            SampleMaker.get_sample(Fsp, X=[0, .3, .7]),
            SampleMaker.get_sample(Fsp, X=[0, .35, .65])
        ]
        resolved_assem_groups = (
            MonophaseAssemblage(unresolved_clustered_samples).segregate_resolved_samples(res))
        assert len(resolved_assem_groups) == 2
